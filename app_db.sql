-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 22, 2016 at 10:55 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `app_db`
--
CREATE DATABASE IF NOT EXISTS `app_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `app_db`;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idea_id` int(11) NOT NULL,
  `user` varchar(100) NOT NULL,
  `comment` varchar(100) NOT NULL,
  `date_posted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `idea_id`, `user`, `comment`, `date_posted`) VALUES
(1, 3, 'Dan', 'cool', '2016-01-21 06:17:58'),
(7, 0, 'kim', 'Excellent idea', '2016-01-21 06:17:58'),
(8, 7, 'kim', 'Good idea', '2016-01-21 06:17:58'),
(9, 3, 'kim', 'MMMMm', '2016-01-21 06:17:58'),
(10, 7, 'kim', 'fsfsdf', '2016-01-21 06:17:58'),
(11, 3, 'Kaput', 'bb', '2016-01-21 06:17:58'),
(12, 3, 'fred', 'aff', '2016-01-21 06:17:58'),
(13, 3, 'kim', '', '2016-01-21 06:17:58'),
(14, 3, 'kim', 'asdsd', '2016-01-21 06:17:58'),
(15, 6, 'fred', 'nice', '2016-01-21 06:17:58'),
(16, 6, 'dotty', 'cool', '2016-01-21 06:53:48'),
(17, 24, 'dotty', 'Interesting', '2016-01-21 07:38:29');

-- --------------------------------------------------------

--
-- Table structure for table `ideas`
--

CREATE TABLE IF NOT EXISTS `ideas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `category` varchar(100) NOT NULL DEFAULT 'Uncategorised',
  `file` varchar(100) NOT NULL DEFAULT 'None',
  `shared` varchar(100) NOT NULL DEFAULT 'personal',
  `posted_by_name` varchar(100) NOT NULL,
  `posted_by_id` varchar(100) NOT NULL,
  `posted_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `team` varchar(100) NOT NULL,
  `rating` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `ideas`
--

INSERT INTO `ideas` (`id`, `title`, `description`, `category`, `file`, `shared`, `posted_by_name`, `posted_by_id`, `posted_on`, `team`, `rating`) VALUES
(3, 'Mobile payment', 'A mobile NFC payment solution', 'other', '', 'Personal', 'Kim', '1', '2016-01-15 10:22:30', 'The Archangel Interactive', 23),
(6, 'CamFind', 'A Camera finder', '', 'FILES', 'Public', 'Diego B', '18', '2016-01-15 10:22:30', 'Microsoft', 28),
(7, 'Data Filter', 'A web based data mining bot', 'Other', '', 'Personal', 'Dotty', '22', '2016-01-15 10:22:30', 'IoT Team', 1),
(8, 'Text Reader', 'A mobile text to sPeech converter', '', 'FILES', 'Personal', 'Ben', '19', '2016-01-15 10:22:30', 'Intel', 6),
(24, 'A polymer collaborative task manager', 'Describes', '', 'FILES/mockup.psd', 'Public', 'Tim', '20', '2016-01-15 13:19:59', 'Simba Tech', 0),
(34, 'Via web', 'A web platform to help uses design and launch their web applications.', 'Other', '', 'Public', 'Dotty', '22', '2016-01-17 09:04:46', 'The Archangel Interactive', 0),
(36, 'Sha-1 App', 'An application to implemet and demo the sha-1 algorithm', 'File encryption', '', 'Personal', 'Dotty', '22', '2016-01-17 09:33:28', 'The Archangel Interactive', 0),
(40, 'Insulin regulator App', 'A blood sugar level monitor to facilitate supplimentation of insulin', 'Other apps', '', 'Personal', 'Vincet Kim', '1', '2016-01-19 16:49:32', 'Safaricom', 0);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` varchar(1000) NOT NULL,
  `sender` varchar(100) NOT NULL,
  `sent_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE IF NOT EXISTS `teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `about` varchar(100) NOT NULL,
  `website` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `name`, `about`, `website`) VALUES
(1, 'The Archangel Interactive ', 'About', 'www.tai.com'),
(2, 'Safaricom', 'the better option', 'www.safaricom.co.ke'),
(3, 'Microsoft', 'about it', 'www.ms.com'),
(4, 'IoT Team', 'about', 'sda.asdsa.com'),
(5, 'Simba Tech', 'About simba tech', 'wwww.simbatech.com'),
(6, 'Mobicomafrika', 'About Mobicom Afrika', 'www.mobicomafrika.com');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `user_level` int(11) NOT NULL,
  `team` varchar(100) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `full_name`, `email`, `username`, `password`, `user_level`, `team`) VALUES
(1, 'Vincet Kim', 'vkim@tai.com', 'kim', '123', 1, 'T.A.I'),
(11, 'Carl Franklin', 'frank@oracle.com', 'frank', '123', 0, 'Oracle'),
(14, 'Fred T', 'fred@firebase.com', 'fred', '123', 0, 'Firebase'),
(15, 'Miguel S', 'miguel@twillio.com', 'Miguel', '123', 0, 'Twillio'),
(16, 'Ken Sanchez', 'ken@iego.com', 'Ken', '123', 0, 'Mongoose Inc'),
(17, 'Harry D', 'harry@gmail.com', 'Harry', '123', 0, 'Google'),
(18, 'Diego B', 'diego@live.com', 'diego', '123', 0, 'Microsoft'),
(19, 'Ben T', 'ben@simbatech.com', 'ben', '123', 0, 'Simba Tech'),
(20, 'Tim Oriley', 'tim@safcom.com', 'tim', '123', 0, 'Safcom'),
(21, 'Clause Dean', 'email@tai.com', 'Clause', '123', 0, 'T.A.I'),
(22, 'Irene Dotty', 'irene@tai.com', 'dotty', '123', 0, 'T.A.I'),
(23, 'Aguero S', 'as@barca.com', 'Sageo', '123', 0, 'Barca'),
(24, 'Zig Zigller ', 'zig@ziginc.com', 'zig', '123', 0, 'Zig Inc');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
