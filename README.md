Clone new repo
create .env file from .env.example
run php artisan key:generate
create database in mysql
set you database variables in .env file
run php artisan migrate --seed in terminal
launch the app
App Features
Register
Login
Create Team
View teams you are a member
View all teams
View a single team
View a single team ideas
View a single team members
Send invitation to registered users
View sent Invitations
View received invitations
Accept invitation to join a team
Leave a team
Post Idea as personal
Post Idea as public
Share idea with a group
View personal Ideas
View public ideas
Logot