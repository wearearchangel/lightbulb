	<?php
/**
 *	Author: Paloma N.
 *	(C) Copyright [your name/company name].
 *  All Rights Reserved.
 *
 *	This php script (is some sort of a database mySQL API that) gets data from db and encodes into json for js.  
 **/

//header("Content-Type: application/json; charset=UTF-8"); 


//capture sent http data
$data=json_decode(file_get_contents("php://input"));

$result = array();
if($data){
	$conn = new mysqli("localhost", "root", "", "app_db");

	$sql_type = $data->params->type;
	$table = $data->params->table;

	switch ($sql_type) {
		case 'select':{
			$sql = "SELECT * FROM ".$table;

			
			//when filter is set
			if(isset($data->params->filter)){
				$filter = $data->params->filter;
				$sql = "SELECT * FROM ".$table." ".$filter;
			}
			if($rows = $conn ->query($sql)){
				while($row = $rows->fetch_assoc()){
					array_push($result, $row);
				}
				echo json_encode(array("items" => $result));
			}else
				echo json_encode($conn->error);
			//$conn->close();
						
		}break;
		case 'insert':{
			$fields = "";
			$values = "";
			$insert_data = $data->params;
			$i =0;
			foreach ($insert_data as $key => $field) {
				//get all relevant fields and values from sent object
				if($key != 'type' && $key != 'table'){
					if($i>0 ){
						$fields .= ",  ".$key;
						$values .= ", '".$field."'";
					}else {
						$fields .= $key;
						$values .= "'".$field."'";
					}
					$i++;
				}
			}
			$sql = "INSERT INTO ".$table."(".$fields.") VALUES(".$values.")";
			$result = $conn ->query($sql);
			echo json_encode($result);
		}break;
		case 'update':{
			$update_data = $data->params->data;
			$sql = "UPDATE ".$table." SET ".$update_data;
			$result = $conn ->query($sql);
			echo json_encode($result);
		}break;
		/*case 'delete':{
			
		}break;
		
		default:
			# code...
			break;*/
	}
}
/*get user's ideas*/

if(isset($_POST['uid'])){
	$uid = $_POST['uid'];
	$conn = new mysqli("localhost", "root", "", "app_db");
	$sql = "SELECT * FROM ideas WHERE posted_by_id = ".$uid;
	$ideas = array();
	if($rows = $conn->query($sql)){
		while ($row = $rows->fetch_assoc()) {
			array_push($ideas, $row);
		}
		echo json_encode($ideas);
	}else
		echo $conn->error;
}
if(isset($_POST['idea_id'])){
	$id = $_POST['idea_id'];
	$conn = new mysqli("localhost", "root", "", "app_db");
	$sql = "SELECT * FROM ideas WHERE id = ".$id;
	$ideas = array();
	if($rows = $conn->query($sql)){
		while ($row = $rows->fetch_assoc()) {
			array_push($ideas, $row);
		}
		echo json_encode($ideas);
	}else
		echo $conn->error;
}

/*post user's ideas*/
//$_POST['idea'] = array("title"=>"asdasd", "description"=>"sfsdf");
if(isset($_POST['title'])){
	$title = $_POST['title'];
	$uid = $_COOKIE['uid'];
	$uname = $_COOKIE['username'];
	$cat = $_POST['category'];
	$desc = $_POST['description'];
	$team = $_POST['team'];
	$shared = $_POST['shared'];
	$file_url = "None";
	if(isset($_POST['file'])){
		$file_name = $_POST['file'];
		$path = $_POST['path'];
      	$file_url="FILES/".$file_name;
      	move_uploaded_file($path, $file_url);
	}
	$sql = "INSERT INTO ideas(title, shared, description, category, posted_by_name, team, posted_by_id, file) 
        VALUES('$title', '$shared', '$desc', '$cat', '$uname', '$team', '$uid', '$file_url')";
    $mysqli = new mysqli("localhost", "root", "", "app_db");
    if($mysqli->query($sql)){     
    	$res = "Successfully posted.";
    }else $res = "Error: ".$mysqli->error;

	echo json_encode($res);
}

/*uPdate user's ideas*/
if(isset($_POST['update_id'])){
	$id = $_POST['update_id'];
	$title = $_POST['update_title'];
	$cat = $_POST['category'];
	$desc = $_POST['description'];
	$team = $_POST['team'];
	$shared = $_POST['shared'];
	$file_url = "";
	if(isset($_POST['file'])){
		$file_name = $_POST['file'];
		$path = $_POST['path'];
      	$file_url="FILES/".$file_name;
      	move_uploaded_file($path, $file_url);
	}
	$sql = "UPDATE ideas SET title = '$title', shared = '$shared', description = '$desc', category = '$cat', team = '$team', file = '$file_url' WHERE id =$id";
    $mysqli = new mysqli("localhost", "root", "", "app_db");
    if($mysqli->query($sql)){     
    	$res = "Successfully Updated.";
    }else $res = "Error: ".$mysqli->error;

	echo json_encode($res);
}