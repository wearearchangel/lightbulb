-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 21, 2016 at 06:34 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `app_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idea_id` int(11) NOT NULL,
  `user` varchar(100) NOT NULL,
  `comment` varchar(100) NOT NULL,
  `date_posted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `idea_id`, `user`, `comment`, `date_posted`) VALUES
(1, 3, 'Dan', 'cool', '2016-01-21 06:17:58'),
(7, 0, 'kim', 'Excellent idea', '2016-01-21 06:17:58'),
(8, 7, 'kim', 'Good idea', '2016-01-21 06:17:58'),
(9, 3, 'kim', 'MMMMm', '2016-01-21 06:17:58'),
(10, 7, 'kim', 'fsfsdf', '2016-01-21 06:17:58'),
(11, 3, 'Kaput', 'bb', '2016-01-21 06:17:58'),
(12, 3, 'fred', 'aff', '2016-01-21 06:17:58'),
(13, 3, 'kim', '', '2016-01-21 06:17:58'),
(14, 3, 'kim', 'asdsd', '2016-01-21 06:17:58'),
(15, 6, 'fred', 'nice', '2016-01-21 06:17:58');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
